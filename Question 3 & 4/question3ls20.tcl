#Create a simulator object
set ns [new Simulator]

#Tell the simulator to use dynamic routing
$ns rtproto LS

$ns color 1 Red
$ns color 2 Orange

#Open the nam trace file
set nf [open question3ls20.nam w]
$ns namtrace-all $nf

#Open the trace file
set tf [open question3ls20.tr w]
$ns trace-all $tf

#Define a 'finish' procedure
proc finish {} {
        global ns nf tf
        $ns flush-trace
	#Close the nam trace file
        close $nf
    #Close the trace file
        close $tf
	#Execute nam on the trace file
        exec nam question3ls20.nam &
        exit 0
}

#Create 10 nodes
for {set i 0} {$i < 20} {incr i} {
        set n($i) [$ns node]
}

#------------Routers-------------#

#Create nodes 0 - 4 in a line
for {set i 0} {$i < 20} {incr i} {
        for {set j [expr $i+1]} {$j < 20} {incr j} {
                $ns duplex-link $n($i) $n($j) 1Mb 10ms DropTail
                $ns at 0.0 "$n($i) label n$i"
                $ns at 0.0 "$n($j) label n$j"
                $n($i) color blue
                $n($j) color blue
        }
}

#------------TCP Connections-------------#

#Setup a TCP connection
set tcp0 [new Agent/TCP]
$tcp0 set class_ 2
$ns attach-agent $n(0) $tcp0
set sink0 [new Agent/TCPSink]
$ns attach-agent $n(11) $sink0
$ns connect $tcp0 $sink0
$tcp0 set fid_ 1

#Setup a FTP over TCP connection
set ftp0 [new Application/FTP]
$ftp0 attach-agent $tcp0
$ftp0 set type_ FTP

# #------------UDP Connections-------------#

#Setup a UDP connection
set udp0 [new Agent/UDP]
$ns attach-agent $n(10) $udp0
set null0 [new Agent/Null]
$ns attach-agent $n(11) $null0
$ns connect $udp0 $null0
$udp0 set fid_ 2

#Setup a CBR over UDP connection
set cbr0 [new Application/Traffic/CBR]
$cbr0 attach-agent $udp0
$cbr0 set type_ CBR
$cbr0 set packet_size_ 1000
$cbr0 set rate_ 2mb
$cbr0 set random_ false

#------------Schedule-------------#

$ns at 0.5 "$ftp0 start"
$ns at 4.0 "$ftp0 stop"

$ns at 2.0 "$cbr0 start"
$ns at 5.5 "$cbr0 stop"

for {set i 11} {$i < 20} {incr i} {
        $ns rtmodel-at 1.[expr $i-10] down $n(0) $n($i)
        $ns rtmodel-at 4.[expr $i-10] up $n(0) $n($i)
        $ns rtmodel-at 2.[expr $i-10] down $n(0) $n([expr $i-10])
        $ns rtmodel-at 5.[expr $i-10] up $n(0) $n([expr $i-10])
}

# #Call the finish procedure after 5 seconds of simulation time
$ns at 6.0 "finish"

#Run the simulation
$ns run