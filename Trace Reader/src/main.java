import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class main {
	public static void main(String[] args) throws IOException{
		
		//Enter trace file location:
		File tr = new File("C:\\Users\\user\\Documents\\Computer Science\\3rd year\\Semester 1\\Data\\ns2\\extension1.tr");
		
		//Enter name of csv file to create:
		FileWriter csv = new FileWriter("extension1.csv");
		
		BufferedReader br = new BufferedReader(new FileReader(tr));
		String str;
		while ((str = br.readLine()) != null){
			String[] arr = str.split(" ");
			if (arr.length > 5 && !(arr[4].equals("ack")) && !(arr[4].equals("rtProtoDV")) && !(arr[4].equals("rtProtoLS"))){
				csv.append(arr[0]);
				csv.append(",");
				csv.append(arr[1]);
				csv.append(",");
				csv.append(arr[2]);
				csv.append(",");
				csv.append(arr[3]);
				csv.append(",");
				csv.append(arr[4]);
				csv.append("\n");
			}
		}
		br.close();
		csv.close();
	}
}
