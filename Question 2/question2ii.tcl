#Create a simulator object
set ns [new Simulator]

#Tell the simulator to use dynamic routing
$ns rtproto LS

$ns color 1 Red
$ns color 2 Orange
$ns color 3 Pink

#Open the nam trace file
set nf [open question2ii.nam w]
$ns namtrace-all $nf

#Open the trace file
set tf [open question2ii.tr w]
$ns trace-all $tf

#Define a 'finish' procedure
proc finish {} {
        global ns nf tf
        $ns flush-trace
	#Close the nam trace file
        close $nf
    #Close the trace file
        close $tf
	#Execute nam on the trace file
        exec nam question2ii.nam &
        exit 0
}

#Create 100 nodes
for {set i 0} {$i < 150} {incr i} {
        set n($i) [$ns node]
}

#Create links between the nodes
#Nodes 0 - 25 are routers
#Nodes 26 - 100 are clients

#------------Routers-------------#

for {set i 0} {$i < 15} {incr i} {
    $ns duplex-link $n(149) $n($i) 100Mb 10ms DropTail
    $ns at 0.0 "$n(149) label ROU$i"
    $n(149) shape square
    $n(149) color blue
}

#Create nodes 0 - 14 in a circle
for {set i 0} {$i < 15} {incr i} {
    $ns duplex-link $n($i) $n([expr ($i+1)%15]) 100Mb 10ms DropTail
    $ns at 0.0 "$n($i) label ROU$i"
    $n($i) shape square
    $n($i) color blue
}

#------------Servers-------------#

for {set i 15} {$i < 30} {incr i} {
    $ns duplex-link $n($i) $n([expr ($i-15)]) 100Mb 10ms DropTail
    $ns at 0.0 "$n($i) label SERVER[expr ($i-15)]"
    $n($i) shape hexagon
    $n($i) color Green
}

#------------Clients-------------#

for {set i 30} {$i < 149} {incr i} {
    $ns duplex-link $n($i) $n([expr (($i+1)%15)+15]) 1Mb 1ms DropTail
    $ns at 0.0 "$n($i) label CL[expr ($i-30)]"
    $n($i) shape circle
    $n($i) color Black
}

#Define a 'recv' function for the class 'Agent/Ping'
Agent/Ping instproc recv {from rtt} {
$self instvar node_
puts "node [$node_ id] received ping answer from \
              $from with round-trip-time $rtt ms."
}

#------------TCP Connections-------------#

#Setup a TCP connection
set tcp0 [new Agent/TCP]
$tcp0 set class_ 2
$ns attach-agent $n(114) $tcp0
set sink0 [new Agent/TCPSink]
$ns attach-agent $n(89) $sink0
$ns connect $tcp0 $sink0
$tcp0 set fid_ 1

#Setup a FTP over TCP connection
set ftp0 [new Application/FTP]
$ftp0 attach-agent $tcp0
$ftp0 set type_ FTP

#Setup a TCP connection
set tcp1 [new Agent/TCP]
$tcp1 set class_ 2
$ns attach-agent $n(137) $tcp1
set sink1 [new Agent/TCPSink]
$ns attach-agent $n(73) $sink1
$ns connect $tcp1 $sink1
$tcp1 set fid_ 1

#Setup a FTP over TCP connection
set ftp1 [new Application/FTP]
$ftp1 attach-agent $tcp1
$ftp1 set type_ FTP

#------------UDP Connections-------------#

#Setup a UDP connection
set udp0 [new Agent/UDP]
$ns attach-agent $n(76) $udp0
set null0 [new Agent/Null]
$ns attach-agent $n(45) $null0
$ns connect $udp0 $null0
$udp0 set fid_ 2

#Setup a CBR over UDP connection
set cbr0 [new Application/Traffic/CBR]
$cbr0 attach-agent $udp0
$cbr0 set type_ CBR
$cbr0 set packet_size_ 1000
$cbr0 set rate_ 2mb
$cbr0 set random_ false

#Setup a UDP connection
set udp1 [new Agent/UDP]
$ns attach-agent $n(122) $udp1
set null1 [new Agent/Null]
$ns attach-agent $n(92) $null1
$ns connect $udp1 $null1
$udp1 set fid_ 2

#Setup a CBR over UDP connection
set cbr1 [new Application/Traffic/CBR]
$cbr1 attach-agent $udp1
$cbr1 set type_ CBR
$cbr1 set packet_size_ 1000
$cbr1 set rate_ 1mb
$cbr1 set random_ false

#------------Pings-------------#

#Create pings
set p0 [new Agent/Ping]
$ns attach-agent $n(62) $p0
set p1 [new Agent/Ping]
$ns attach-agent $n(34) $p1
set p2 [new Agent/Ping]
$ns attach-agent $n(89) $p2
set p3 [new Agent/Ping]
$ns attach-agent $n(106) $p3

#Connect the two agents
$ns connect $p0 $p1
$ns connect $p2 $p3

#------------Schedule-------------#

#Schedule events for the CBR agent and the network dynamics

$ns at 0.5 "$ftp0 start"
$ns at 5.0 "$ftp0 stop"
$ns at 1.0 "$ftp1 start"
$ns at 3.0 "$ftp1 stop"

$ns at 2.0 "$cbr0 start"
$ns at 4.5 "$cbr0 stop"
$ns at 2.5 "$cbr1 start"
$ns at 5.0 "$cbr1 stop"

$ns at 0.2 "$p0 send"
$ns at 0.4 "$p1 send"
$ns at 2.6 "$p2 send"
$ns at 4.7 "$p3 send"
$ns at 2.2 "$p0 send"
$ns at 3.4 "$p1 send"
$ns at 1.6 "$p2 send"
$ns at 3.7 "$p3 send"

#Call the finish procedure after 5 seconds of simulation time
$ns at 6.0 "finish"

#Run the simulation
$ns run