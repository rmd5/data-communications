#Create a simulator object
set ns [new Simulator]
#Open a nam trace file
set nf [open lab3iii.nam w]
$ns namtrace-all $nf

set tf [open lab3iii.tr w]
$ns trace-all $tf
#Define a 'finish' procedure
proc finish {} {
global ns nf tf
$ns flush-trace
close $nf
close $tf
exec nam out.nam &
exit 0
}
set n0 [$ns node]
set n1 [$ns node]
set n2 [$ns node]
$n0 color blue
$n1 color red
$n2 color green
#Connect the nodes with two links
$ns duplex-link $n0 $n1 1Mb 10ms DropTail
$ns duplex-link $n2 $n1 0.5Mb 10ms DropTail
proc smtp_traffic1 {node0 node1} { 
global ns
set smtp_UDP_agent [new Agent/UDP]
set smtp_UDP_sink [new Agent/UDP]
$ns attach-agent $node0 $smtp_UDP_agent
$ns attach-agent $node1 $smtp_UDP_sink
$ns connect $smtp_UDP_agent $smtp_UDP_sink
set smtp_UDP_source [new Application/Traffic/Exponential]
$smtp_UDP_source attach-agent $smtp_UDP_agent
$smtp_UDP_source set packetSize_ 210
$smtp_UDP_source set burst_time_ 50ms
$smtp_UDP_source set idle_time_ 50ms
$smtp_UDP_source set rate_ 100k
$ns at 0.2 "$smtp_UDP_source start"
$ns at 3.2 "$smtp_UDP_source stop"
}
proc smtp_traffic2 {node1 node2} { 
global ns
set smtp_UDP_agent [new Agent/UDP]
set smtp_UDP_sink [new Agent/UDP]
$ns attach-agent $node2 $smtp_UDP_agent
$ns attach-agent $node1 $smtp_UDP_sink
$ns connect $smtp_UDP_agent $smtp_UDP_sink
set smtp_UDP_source [new Application/Traffic/Exponential]
$smtp_UDP_source attach-agent $smtp_UDP_agent
$smtp_UDP_source set packetSize_ 430
$smtp_UDP_source set burst_time_ 20ms
$smtp_UDP_source set idle_time_ 30ms
$smtp_UDP_source set rate_ 200k
$ns at 2.0 "$smtp_UDP_source start"
$ns at 4.0 "$smtp_UDP_source stop"
}
smtp_traffic1 $n0 $n1
smtp_traffic2 $n1 $n2
$ns at 4.0 "finish"
$ns run 
